filnavn = input('Oppgi navn på fila: ')

print(f'Skriv inn tekst som lagres til {filnavn}')
print('Avslutt med å trykke enter (uten tekst)')
tekst ='noe'# For at while-lokka skal få True
enter ='\n' # Tegn for ny linje
liste =[]   # Lager en tom liste

while tekst!='':
    tekst = input('> ')
    if tekst!='':
        liste.append(tekst+enter)

f = open(filnavn,'w') # Åpner fila for skriving
f.writelines(liste)   # Skriver lista av strenger til fil
f.close()             # Stenger fila

print(f'Teksten har nå blitt lagret i {filnavn}')