def romersiffer (n, entegn, femtegn, titegn):
    # returnerer romertall som svarer til n med spesifiserte tegn for 1, 5 og 10
    # Dette gjelder uansett om vi ser på enere, tiere eller hundrere -
    # prinsippet er akkurat det samme. Det eneste som er ulikt er tegnene vi bruker.
    if n==0: return ''
    elif n==1: return entegn
    elif n==2: return entegn+entegn
    elif n==3: return entegn+entegn+entegn
    elif n==4: return entegn+femtegn
    elif n==5: return femtegn
    elif n==6: return femtegn+entegn
    elif n==7: return femtegn+entegn+entegn
    elif n==8: return femtegn+entegn+entegn+entegn
    elif n==9: return entegn+titegn
    else: return 'ERROR!!!!!'

def romertall(n):
    # returere romertall fra 0 til 999
    r = '' # romertallet som streng
    if n>999 or n<0: return 'Tallet for stort eller lite'
    elif n>99:
        # Nå må vi finne ut hvor mange hundrere det er i tallet
        # n // 100 gir svaret!
        # Vi legger til romersiffer(svaret av hundrerne som tall, og tegnene for
        # 100, 500 og 1000)
        r = r +romersiffer(n//100,'C','D','M')
        # Med n = 952 vil en her kalle romersiffer med 900, og r blir 'CM'

        # Nå må vi trekke fra hundrene
        # Hvis n egentlig er 952 vil neste linje omgjøre den til 52
        n = n - (n//100)*100 # Fjerner hundreplassen
        
    # Utvider strengen med romersiffer, nå med hvor mange tiere vi har
    # 52//10 = 5. Siden det er tierne vi ser på må vi kalle romersiffer
    # med tegnene 'X', 'L' og 'C'
    r = r + romersiffer(n//10,'X','L','C') # Tierplassen
    # Starter vi med tallet 952 vil vi her ende opp med r = 'CM' + 'L' = 'CML'

    # Siste del: sette inn enerne. Hvor mange? n%10 gir resten:
    # 52%10 gir 2. 
    r = r + romersiffer(n%10,'I','V','X') # Enerplassen
    # Starter vi med tallet 952 vil vi her ende opp med r = 'CML' + 'II' = 'CMLII'
    return r # returnerer hele strengen. inn 952, ut 

n = 0
while n>=0:
    n = int(input('Tall mellom 0 og 999 '))
    r = romertall(n)
    print('Romertall for ',n,' er',r)
    

